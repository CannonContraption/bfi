# BFI - an interpreter for the language Brainfuck

This whole project was a pointless 1AM hack that I modified a couple times. All
versions should work, and work fairly efficiently. They differ in how they load
programs.

## bfi - the Original

This one was about 70% implemented between the hours of 1 and 2 AM. Everything
but the code to actually load a program was done by the time 2AM hit.

This version loads programs by reading the file, allocating extents of program
buffer memory as it reads. When it has reached the end of the file, and it has
read all the valid program characters into these buffers, it then allocates a
new string exactly the size of the valid parts of the program, stores the
program there for easy running, and frees the program load buffers from memory.

## bfi-lowmem - I decide it's stupid to allocate memory for the size of the program twice

This version keeps most of the 2AM code (except for the loops) and reimplements
the code from the day after so that no final program string is allocated and the
program load buffers are used directly. This design necessitated the loop code
change since the loops stored a program counter on a stack, and keeping a
program counter is not as straightforward as a buffer index and a slab pointer
when using slabs of memory in a list rather than one contiguous program space.

Note that at the moment this version suffers from a bug where it does not
execute '[' instructions properly- it does not skip them if head = 0.

## bfi-slab - Another approach to loader memory usage reduction

This version allocates a slab of memory the size of the file, then reads and
validates every character as it comes in and stores the valid ones. This has the
effect of wasting any memory used by non-instruction characters, but in theory
could make it the fastest and lightest of the three if its input is a program
with no comments added.

# Rationale
I wanted to write these, so I did.

# Writing Programs in Brainfuck

As a note, Brainfuck is an informal variant of the theoretical language P''.

Brainfuck is a stupidly simple language. I can describe its use with one table
and a brief introduction to memory management in a Turing Machine.

## Turing Machines
Turing Machines handle memory in a similar-but-different way compared to regular
computers we use today. Memory is conceptualized as an infinitely-large tape,
divided into cells, which can be read one value at a time. Which value you can
read is determined by the position of the tape head. Turing machines also have a
"state register", but that isn't strictly relevant to Brainfuck. Turing machines
also have a finitely-long program, or table of instructions.

## Brainfuck Instructions

| Instruction | Description                                          |
|-------------|------------------------------------------------------|
| +           | Increment the tape pointer                           |
| -           | Decrement the tape pointer                           |
| >           | Move the tape pointer to the right one cell          |
| <           | Move the tape pointer to the left one cell           |
| [           | begin a loop (or skip it if head = 0)                |
| ]           | Go back to the beginning of the loop unless head = 0 |

## Example program

This example writes the letter 'd' on screen and exits. Note that no newline
character is printed:

    ++++++++++[<++++++++++>-]<.

This program increments the value under the head 10 times, moves the tape
pointer to the left once, increments the value under the head 10 times, moves
the head back to the right one cell, then decrements the value in that cell
once, and if the resulting value isn't zero yet, goes back to where it first
moved the head until it sees zero. It then prints the value under the head to
the screen. Note that the number 100 (what we counted up to by the end of the
program) is an ASCII lowercase 'd'.

## Comments

By the spec of the language, every character that is not an instruction is to be
ignored, and is therefore a comment. Therefore, this readme is technically a
valid Brainfuck program, although since there is a period before any other valid
instruction to change the value under the head, the very first thing it would
print to the screen is \0. It also would never exit, since there are more
hyphens than +'s, and so the table of example values would put it into an
infinite loop.

If you want a real example program, there is at least one provided with this
software.
