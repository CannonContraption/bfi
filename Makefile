default: bin/bfi
lowmem: bin/bfi-lowmem
slab: bin/bfi-slab

bin:
	mkdir bin

bin/bfi: bin bfi.c
	gcc bfi.c -o bin/bfi

bin/bfi-lowmem: bin bfi-lowmem.c
	gcc bfi-lowmem.c -o bin/bfi-lowmem

bin/bfi-slab: bin bfi-slab.c
	gcc bfi-slab.c -o bin/bfi-slab

