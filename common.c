
/*
 _______  __ _  __ _  ___  ___   ____
/ __/ _ \/  ' \/  ' \/ _ \/ _ \_/ __/
\__/\___/_/_/_/_/_/_/\___/_//_(_)__/

Takes care of common tasks to the interpreters. This mainly includes things to
do with tape extents, since across all implementations (so far) the tape behaves
exactly the same. There is, therefore, no need to implement this in three
different files.
 */

/*
===== STATIC DEFINES ===========================================================
*/
#define EXTENT_SIZE 20
#define EXTENT_MAX  (EXTENT_SIZE - 1)

/*
===== COMMON STRUCTURES ========================================================
*/
/*
 Brainfuck works like a turing machine, but nothing else does.
 Create a struct to hold an extent of the Turing memory
*/
typedef struct s_extent
{
  int               buffer[EXTENT_SIZE]; /* Raw data buffer */
  int               position;            /* Position in the buffer */
  struct s_extent * next;                /* Pointer to next extent */
  struct s_extent * previous;            /* Pointer to last extent */
} extent;

/*
===== HEADER INFORMATION =======================================================
*/
extent * generate_new_extent(
    extent * next,
    extent * prev,
    int      position);
void print_head(
    extent * tape_head);
void read_head(
    extent * tape_head);
void increment(
    extent * tape_head);
void decrement(
    extent * tape_head);
extent * pointer_forward(
    extent * tape_head);
extent * pointer_backward(
    extent * tape_head);
void extent_cleanup(
    extent * tape_head);
void extent_clean_reverse(
    extent * tape_head);
void extent_clean_forward(
    extent * tape_head);
extent * init_machine();

/*
===== FUNCTION IMPLEMENTATION ==================================================
*/

extent * init_machine()
{
  extent * starting_point = generate_new_extent(
      NULL,
      NULL,
      EXTENT_SIZE / 2);
  return starting_point;
}

/*
  extent_clean_forward()
  
  frees all memory forward from tape head.
*/
void extent_clean_forward(
    extent * tape_head)
{
  if (tape_head != NULL)
    {
      extent * extent_next = tape_head -> next;
      free(tape_head);
      extent_clean_forward(
          extent_next);
    }
}

/*
  extent_clean_reverse()
  
  frees all memory forward from tape head.
*/
void extent_clean_reverse(
    extent * tape_head)
{
  if (tape_head != NULL)
    {
      extent * extent_next = tape_head -> previous;
      free(tape_head);
      extent_clean_reverse(
          extent_next);
    }
}

/*
  extent_cleanup()

  Cleans up the tape, returning entire memory space to the host OS.
*/
void extent_cleanup(
    extent * tape_head)
{
  if (tape_head != NULL)
    {
      extent * extent_next = tape_head -> next;
      extent_clean_forward(
          extent_next);
      extent_clean_reverse(
          tape_head);
    }
  else
    {
      fprintf(
          stderr,
          "\033[1;31mTried to clean up an empty machine! Should never happen!\033[m\n");
    }
}

/*
  generate_new_extent()

  Quick function to handle the repetitive tasks associated with creating a new
  extent. Should only be called when access is requested beyond bounds or
  program execution has just started.
 */
extent * generate_new_extent(
    extent * next,
    extent * prev,
    int      position)
{
  extent * new_extent = malloc(sizeof(extent));

  new_extent -> position = position;

  for (int segment = 0; segment < EXTENT_SIZE; segment ++)
    {
      new_extent->buffer[segment] = 0;
    }

  new_extent -> next     = next;
  new_extent -> previous = prev;

  return new_extent;
}

/*
  print_head()

  Prints the tape head
*/
void print_head(
    extent * tape_head)
{
  int tape_current = tape_head -> buffer[tape_head->position];
  if (tape_current != 10)
    putchar(
        tape_head -> buffer[tape_head->position]);
  else
    putchar('\n');
}

/*
  read_head()

  Reads a character off the screen and writes it to tape
*/
void read_head(
    extent * tape_head)
{
  int input_char = getchar();
  if (input_char == '\n')
    {
      tape_head -> buffer [tape_head -> position] = 10;
    }
  else if(input_char != EOF)
    {
      tape_head -> buffer [tape_head -> position] = input_char;
    }
}

/*
  increment()

  Increment the tape head.
*/
void increment(
    extent * tape_head)
{
  tape_head -> buffer[tape_head->position] ++;
}

/*
  decrement()

  Decrement the tape head.
*/
void decrement(
    extent * tape_head)
{
  tape_head -> buffer[tape_head->position] --;
}

/*
  pointer_forward()
  
  Advances the tape pointer, creating a new extent if needed
*/
extent * pointer_forward(
    extent * tape_head)
{
  if (tape_head -> position == EXTENT_MAX)
    {
      if (tape_head -> next == NULL)
        {
          tape_head = generate_new_extent(
              NULL,      /* Next (we're last in the chain) */
              tape_head, /* Previous (what was current) */
              0);        /* We start at the beginning of this extent */
        }
      else
        {
          tape_head = tape_head -> next;
        }
    }
  else
    {
      tape_head -> position ++;
    }
  return tape_head;
}

/*
  pointer_backward()
  
  Reverses the tape pointer, creating a new extent if needed
*/
extent * pointer_backward(
    extent * tape_head)
{
  if (tape_head -> position == 0)
    {
      if (tape_head -> previous == NULL)
        {
          tape_head = generate_new_extent(
              tape_head,   /* Next (what was current) */
              NULL,        /* Previous (this is first in the chain) */
              EXTENT_MAX); /* Starting point (we're on the last element) */
        }
      else
        {
          tape_head = tape_head -> previous;
        }
    }
  else
    {
      tape_head -> position --;
    }
  return tape_head;
}
