/*
   __   ____         __     __
  / /  / _(_)______ / /__ _/ /   ____
 / _ \/ _/ /___(_-</ / _ `/ _ \_/ __/
/_.__/_//_/   /___/_/\_,_/_.__(_)__/

Allocates a slab of memory, reads the file into it. See README for details.
 */


/*
===== INCLUDES =================================================================
*/
#include<stdio.h>
#include<stdlib.h>
#include"common.c"


/*
===== STATIC DEFINES ===========================================================
*/
#define PROGRAM_BUFF_EXTENT_SIZE 200


/*
===== STRUCTURES ===============================================================
*/
typedef struct s_loop
{
  int             position;  /* Index into the program tape */
  struct s_loop * superloop; /* Pointer to any outside loops */
} loop;


/*
===== FUNCTION IMPLEMENTATION ==================================================
*/
/*
  Program validation code.

  Strips out all characters that are comments as per the brainfuck spec. That
  is, all non-program characters should be identified as 'ignored', and this
  takes care of that.
*/
int is_valid_instruction_char(
    char instruction)
{
  switch(instruction)
    {
    case '<': /* All expected fall-through */
    case '>':
    case '.':
    case ',':
    case '+':
    case '-':
    case '[':
    case ']':
      return 1;
    }
  return 0;
}

/*
  File open code.

  Loads a program into memory, validates every incoming character, then loads it
  into a single string for reading by the interpreter.
 */
char * open_file(
    char * filename)
{
  FILE * program_file = fopen(
      filename,
      "r");
  fseek(
      program_file,
      0L,
      SEEK_END);
  int    progsize = ftell(
      program_file);
  rewind(
      program_file);

  char * program = calloc(
      progsize,
      1);
  char   program_char;
  int    program_cursor = 0;

  while(!feof(program_file) && !ferror(program_file))
    {
      program_char = getc(program_file);
      if (is_valid_instruction_char(program_char))
        {
          program[program_cursor] = program_char;
          program_cursor ++;
        }
    }

  return program;
}

/*
  loop_set_start()

  Sets the start point for the current loop.
*/
loop * loop_set_start(
    loop   * parent_loop,
    int      pc)
{
  loop * current_loop = malloc(sizeof(loop));
  current_loop -> superloop = parent_loop;
  current_loop -> position  = pc;

  return current_loop;
}

/*
  loop_end()

  Returns back to the start of the loop, decrements tape head to match. Does not
  modify PC if the loop is done.
*/
int loop_end(
    loop   ** current_loop,
    extent  * tape_head,
    int       pc)
{
  if (tape_head -> buffer[tape_head->position] == 0)
    {
      loop * old_loop = *current_loop;
      (*current_loop) = (*current_loop) -> superloop;
      free(old_loop);
      return pc;
    }
  return (*current_loop) -> position;
}

int main(
    int    argc,
    char * argv[])
{
  if (argc < 1)
    {
      fputs(
          "MUST specify filename as argument 1!",
          stderr);
    }
  extent * tape_head       = init_machine();
  loop   * loop_stack      = NULL;
  char   * program         = open_file(argv[1]);
  int      program_counter = 0;

  while (program[program_counter] != '\0')
    {
      switch (program[program_counter])
        {
        case '+':
          increment(
              tape_head);
          break;
        case '-':
          decrement(
              tape_head);
          break;
        case '<':
          tape_head = pointer_backward(
              tape_head);
          break;
        case '>':
          tape_head = pointer_forward(
              tape_head);
          break;
        case '[':
          if (b == 0)
            {
              while (program[program_counter] != '\0')
                {
                  if (program[program_counter] == '[')
                    {
                      loop_scount ++;
                    }
                  else if (program[program_counter] == ']')
                    {
                      loop_scount --;
                    }
                  if (!loop_scount)
                    {
                      break;
                    }
                  else
                    {
                      program_counter ++;
                    }
                }
            }
          else
            {
              loop_stack = loop_set_start(
                  loop_stack,
                  program_counter);
            }
          break;
        case ']':
          program_counter = loop_end(
              &loop_stack,
              tape_head,
              program_counter);
          break;
        case '.':
          print_head(
              tape_head);
          break;
        case ',':
          read_head(
              tape_head);
          break;
        default:
          fputs(
              "CRITICAL INTERNAL ERROR!!! Invalid instruction",
              stderr);
          return 1;
          break;
        }
      program_counter ++;
    }

  free(program);

  extent_cleanup(
      tape_head);
  return 0;
}
