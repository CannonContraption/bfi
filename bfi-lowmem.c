/*
   __   ____     __
  / /  / _(_)___/ /__ _    ____ _  ___ __ _   ____
 / _ \/ _/ /___/ / _ \ |/|/ /  ' \/ -_)  ' \_/ __/
/_.__/_//_/   /_/\___/__,__/_/_/_/\__/_/_/_(_)__/

The low-memory version that shouldn't probably even exist. See README for more
details.
*/


/*
===== INCLUDES =================================================================
*/
#include<stdio.h>
#include<stdlib.h>
#include"common.c"

/*
===== STATIC DEFINES ===========================================================
*/
#define PROGRAM_BUFF_EXTENT_SIZE 200

/*
===== STRUCTURES ===============================================================
*/
/*
  Program buffer extent. Used to house parts of the program file during code
  load while the file is being read and verified. Created to allow dynamic,
  theoretically infinite program sizes.
*/
typedef struct s_program_load_extent
{
  char                           buffer[PROGRAM_BUFF_EXTENT_SIZE];
  struct s_program_load_extent * next;
  int                            cursor;
  int                            length;
} program_load_extent;

typedef struct s_loop
{
  int             position;  /* Index into the program tape */
  struct s_loop * superloop; /* Pointer to any outside loops */
  program_load_extent * program_extent;
} loop;



/*
===== FUNCTION IMPLEMENTATION ==================================================
*/
/*
  Program validation code.

  Strips out all characters that are comments as per the brainfuck spec. That
  is, all non-program characters should be identified as 'ignored', and this
  takes care of that.
*/
int is_valid_instruction_char(
    char instruction)
{
  switch(instruction)
    {
    case '<': /* All expected fall-through */
    case '>':
    case '.':
    case ',':
    case '+':
    case '-':
    case '[':
    case ']':
      return 1;
    }
  return 0;
}

/*
  File open code.

  Loads a program into memory, validates every incoming character, then loads it
  into a single string for reading by the interpreter.
 */
program_load_extent * open_file(
    char * filename)
{
  FILE * program_file = fopen(
      filename,
      "r");

  program_load_extent * program_master_head;
  program_load_extent * program_head = malloc(sizeof(program_load_extent));
  program_head -> cursor = 0;
  program_head -> length = 0;
  program_head -> next   = NULL;
  program_master_head = program_head;

  char program_char;

  while(!feof(program_file) && !ferror(program_file))
    {
      program_char = getc(program_file);
      if (is_valid_instruction_char(program_char))
        {
          program_head -> buffer[program_head->cursor] = program_char;
          program_head -> cursor ++;
          program_head -> length ++;
          if (program_head -> length == PROGRAM_BUFF_EXTENT_SIZE)
            {
              program_head -> next = malloc(sizeof(program_load_extent));
              program_head -> cursor = 0;
              program_head = program_head -> next;

              program_head -> cursor = 0;
              program_head -> next   = NULL;
              program_head -> length = 0;
            }
        }
    }

  program_head = program_master_head;
  int program_size = 0;

  while (program_head)
    {
      program_size += program_head->cursor;
      program_head  = program_head->next;
    }

  return program_master_head;
}
void clear_program_space(
    program_load_extent * program_master_head)
{
  program_load_extent * program_head = program_master_head;

  program_head = program_master_head;
  while (program_master_head)
    {
      /* Assumes the program list is null terminated */
      program_master_head = program_head -> next;
      free(program_head);
      program_head = program_master_head;
    }
}

/*
  loop_set_start()

  Sets the start point for the current loop.
*/
loop * loop_set_start(
    loop   * parent_loop,
    program_load_extent * progext,
    int      pc)
{
  loop * current_loop = malloc(sizeof(loop));
  current_loop -> superloop = parent_loop;
  current_loop -> position  = pc;
  current_loop -> program_extent = progext;

  return current_loop;
}

/*
  loop_end()

  Returns back to the start of the loop, decrements tape head to match. Does not
  modify PC if the loop is done.
*/
program_load_extent * loop_end(
    loop   ** current_loop,
    extent  * tape_head)
{
  if (tape_head -> buffer[tape_head->position] == 0)
    {
      loop * old_loop = *current_loop;
      (*current_loop) = (*current_loop) -> superloop;
      free(old_loop);
      return NULL;
    }
  (*current_loop) -> program_extent -> cursor = (*current_loop) -> position;
  return (*current_loop) -> program_extent;
}

int main(
    int    argc,
    char * argv[])
{
  if (argc < 1)
    {
      fputs(
          "MUST specify filename as argument 1!",
          stderr);
    }
  extent                * tape_head       = init_machine();
  loop                  * loop_stack      = NULL;
  program_load_extent   * program         = open_file(argv[1]);
  program_load_extent   * master_program  = program;
  program_load_extent   * loop_program;

  program->cursor = 0;

  while (program)
    {
      switch (program->buffer[program->cursor])
        {
        case '+':
          increment(
              tape_head);
          break;
        case '-':
          decrement(
              tape_head);
          break;
        case '<':
          tape_head = pointer_backward(
              tape_head);
          break;
        case '>':
          tape_head = pointer_forward(
              tape_head);
          break;
        case '[':
          loop_stack = loop_set_start(
              loop_stack,
              program,
              program -> cursor);
          break;
        case ']':
          loop_program = loop_end(
              &loop_stack,
              tape_head);
          if (loop_program)
            {
              program = loop_program;
            }
          break;
        case '.':
          print_head(
              tape_head);
          break;
        case ',':
          read_head(
              tape_head);
          break;
        default:
          fprintf(
              stderr,
              "CRITICAL INTERNAL ERROR!!! Invalid instruction '%c'\n",
              program->buffer[program->cursor]);
          return 1;
          break;
        }
      program -> cursor ++;
      if (program -> cursor == (program -> length) ||
          (program->buffer[program->cursor] == '\0'))
        {
          program = program -> next;
          if (!program)
            {
              return 0;
            }
          program -> cursor = 0;
          if (program->length == 0)
            {
              return 0;
            }
        }
    }

  clear_program_space(
      master_program);

  extent_cleanup(
      tape_head);
  return 0;
}
