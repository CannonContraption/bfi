/*
   __   ____
  / /  / _(_)____
 / _ \/ _/ // __/
/_.__/_//_(_)__/

The original.
*/


/*
===== INCLUDES =================================================================
*/
#include<stdio.h>
#include<stdlib.h>
#include"common.c"


/*
===== STATIC DEFINES ===========================================================
*/
#define PROGRAM_BUFF_EXTENT_SIZE 200


/*
===== STRUCTURES ===============================================================
*/
/*
  Program buffer extent. Used to house parts of the program file during code
  load while the file is being read and verified. Created to allow dynamic,
  theoretically infinite program sizes.
*/
typedef struct s_program_load_extent
{
  char                           buffer[PROGRAM_BUFF_EXTENT_SIZE];
  struct s_program_load_extent * next;
  int                            cursor;
} program_load_extent;

typedef struct s_loop
{
  int             position;  /* Index into the program tape */
  struct s_loop * superloop; /* Pointer to any outside loops */
} loop;


/*
===== FUNCTION IMPLEMENTATION ==================================================
*/
/*
  Program validation code.

  Strips out all characters that are comments as per the brainfuck spec. That
  is, all non-program characters should be identified as 'ignored', and this
  takes care of that.
*/
int is_valid_instruction_char(
    char instruction)
{
  switch(instruction)
    {
    case '<': /* All expected fall-through */
    case '>':
    case '.':
    case ',':
    case '+':
    case '-':
    case '[':
    case ']':
      return 1;
    }
  return 0;
}

/*
  File open code.

  Loads a program into memory, validates every incoming character, then loads it
  into a single string for reading by the interpreter.
 */
char * open_file(
    char * filename)
{
  FILE * program_file = fopen(
      filename,
      "r");

  program_load_extent * program_master_head;
  program_load_extent * program_head = malloc(sizeof(program_load_extent));
  program_head -> cursor = 0;
  program_head -> next   = NULL;
  program_master_head = program_head;

  char program_char;

  while(!feof(program_file) && !ferror(program_file))
    {
      program_char = getc(program_file);
      if (is_valid_instruction_char(program_char))
        {
          program_head -> buffer[program_head->cursor] = program_char;
          program_head -> cursor ++;
          if (program_head -> cursor == PROGRAM_BUFF_EXTENT_SIZE)
            {
              program_head -> next = calloc(sizeof(program_load_extent), 1);
              program_head = program_head -> next;

              program_head -> cursor = 0;
              program_head -> next   = NULL;
            }
        }
    }

  program_head = program_master_head;
  int program_size = 0;

  while (program_head)
    {
      program_size += program_head->cursor;
      program_head  = program_head->next;
    }

  char * program        = malloc(program_size+1);
  int    program_cursor = 0;

  program_head = program_master_head;

  while (program_head)
    {
      for (int instruction = 0; instruction < program_head->cursor; instruction ++)
        {
          program[program_cursor] = program_head->buffer[instruction];
          program_cursor ++;
        }
      program_head = program_head -> next;
    }

  program[program_size] = '\0';

  program_head = program_master_head;
  while (program_master_head)
    {
      program_master_head = program_head -> next;
      free(program_head);
      program_head = program_master_head;
    }

  return program;
}

/*
  loop_set_start()

  Sets the start point for the current loop.
*/
loop * loop_set_start(
    loop   * parent_loop,
    int      pc)
{
  loop * current_loop = malloc(sizeof(loop));
  current_loop -> superloop = parent_loop;
  current_loop -> position  = pc;

  return current_loop;
}

/*
  loop_end()

  Returns back to the start of the loop, decrements tape head to match. Does not
  modify PC if the loop is done.
*/
int loop_end(
    loop   ** current_loop,
    extent  * tape_head,
    int       pc)
{
  if (tape_head -> buffer[tape_head->position] == 0)
    {
      loop * old_loop = *current_loop;
      (*current_loop) = (*current_loop) -> superloop;
      free(old_loop);
      return pc;
    }
  return (*current_loop) -> position;
}

int main(
    int    argc,
    char * argv[])
{
  if (argc < 1)
    {
      fputs(
          "MUST specify filename as argument 1!",
          stderr);
    }
  extent * tape_head       = init_machine();
  loop   * loop_stack      = NULL;
  char   * program         = open_file(argv[1]);
  int      program_counter = 0;
  int      loop_scount     = 0;
  int      b               = 0;

  while (program[program_counter] != '\0')
    {
      b = tape_head -> buffer[tape_head -> position];
      switch (program[program_counter])
        {
        case '+':
          increment(
              tape_head);
          break;
        case '-':
          decrement(
              tape_head);
          break;
        case '<':
          tape_head = pointer_backward(
              tape_head);
          break;
        case '>':
          tape_head = pointer_forward(
              tape_head);
          break;
        case '[':
          if (b == 0)
            {
              while (program[program_counter] != '\0')
                {
                  if (program[program_counter] == '[')
                    {
                      loop_scount ++;
                    }
                  else if (program[program_counter] == ']')
                    {
                      loop_scount --;
                    }
                  if (!loop_scount)
                    {
                      break;
                    }
                  else
                    {
                      program_counter ++;
                    }
                }
            }
          else
            {
              loop_stack = loop_set_start(
                  loop_stack,
                  program_counter);
            }
          break;
        case ']':
          program_counter = loop_end(
              &loop_stack,
              tape_head,
              program_counter);
          break;
        case '.':
          print_head(
              tape_head);
          break;
        case ',':
          read_head(
              tape_head);
          break;
        default:
          fputs(
              "CRITICAL INTERNAL ERROR!!! Invalid instruction",
              stderr);
          return 1;
          break;
        }
      program_counter ++;
    }

  free(program);

  extent_cleanup(
      tape_head);
  return 0;
}
