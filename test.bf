++++++++++[<++++++++++>-]<. (prints a d)
+++>+++>+++>+++>+++>++++>++>++++>++>+++++>+++>++++++>+++++++>++++++++++++[>++++<-]>. (print 0)
---<---<-- <-  <-- <- - <- <-   <- <-----<   <      <       <- - - - -            <  (restore to start)
+++<+++<+++<+++<+++<++++<++<++++<++<+++++<+++<++++++<+++++++<++++++++++++[<++++>-]<. (print 0)

This sentence has no punctuation since that would not be a comment
Always comment your code, kids.
